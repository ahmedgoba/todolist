package goba;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;

public class GobaDatabase {
	
	String jdbcUrl ="jdbc:mysql://localhost:3306/goba?autoReconnect=true&useSSL=false";
    String userid = "goba";
    String password = "0000"; 
    Connection conn;
    Statement stmt;
    ResultSet rset;
    String query;
    
    public void getDBConnection() throws SQLException{
    	 try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	conn=DriverManager.getConnection(jdbcUrl,userid,password);  
    }
    
    public ResultSet getDailyEvents() throws SQLException{
        getDBConnection(); 
        stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
               ResultSet.CONCUR_READ_ONLY);
        query = "SELECT * FROM Daily_Events";
        //System.out.println("\nExecuting query: " + query);
        rset = stmt.executeQuery(query); 
        return rset;
    }
    public ResultSet getCustomEvents() throws SQLException{
        getDBConnection(); 
        stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
               ResultSet.CONCUR_READ_ONLY);
        query = "SELECT * FROM Custom_Events";
        //System.out.println("\nExecuting query: " + query);
        rset = stmt.executeQuery(query); 
        return rset;
    }
    public ResultSet getCompletedEvents() throws SQLException{
        getDBConnection(); 
        stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
               ResultSet.CONCUR_READ_ONLY);
        query = "SELECT * FROM Completed_Events";
        //System.out.println("\nExecuting query: " + query);
        rset = stmt.executeQuery(query); 
        return rset;
    }
    public void AddToDailyEvents(Event event) throws SQLException{
        getDBConnection(); 
        query = "INSERT INTO Daily_Events (name,hasReminder,day,hour,minute) VALUES (?,?,?,?,?)";
        PreparedStatement preStatement =conn.prepareStatement(query);
        preStatement.setString(1, event.getEventDisc());
        preStatement.setBoolean(2, event.getHasTime());
        if(event.getEventDate()==null)
        	 preStatement.setDate(3,null);
        else
        preStatement.setDate(3,new java.sql.Date( event.getEventDate().getTime()));
        preStatement.setInt(4, event.getHour());
        preStatement.setInt(5, event.getMinute());
        int result = preStatement.executeUpdate();
    }
    public void AddToCustomEvents(Event event) throws SQLException{
        getDBConnection(); 
        query = "INSERT INTO Custom_Events (name,hasReminder,day,hour,minute) VALUES (?,?,?,?,?)";
        PreparedStatement preStatement =conn.prepareStatement(query);
        preStatement.setString(1, event.getEventDisc());
        preStatement.setBoolean(2, event.getHasTime());
        if(event.getEventDate()==null)
        	preStatement.setDate(3,null);
        else
        	preStatement.setDate(3,new java.sql.Date( event.getEventDate().getTime()));
        preStatement.setInt(4, event.getHour());
        preStatement.setInt(5, event.getMinute());
        int result = preStatement.executeUpdate();
    }
    public void AddToCompletedEvents(Event event) throws SQLException{
        getDBConnection(); 
        query = "INSERT INTO Completed_Events (name,hasReminder,day,hour,minute) VALUES (?,?,?,?,?)";
        PreparedStatement preStatement =conn.prepareStatement(query);
        preStatement.setString(1, event.getEventDisc());
        preStatement.setBoolean(2, event.getHasTime());
        if(event.getEventDate()==null)
        	preStatement.setDate(3,null);
        else
        	preStatement.setDate(3,new java.sql.Date( event.getEventDate().getTime()));
        preStatement.setInt(4, event.getHour());
        preStatement.setInt(5, event.getMinute());
        int result = preStatement.executeUpdate();
    }
    public ResultSet getEventFromDailyEvents(int id) throws SQLException{
        getDBConnection(); 
        stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
               ResultSet.CONCUR_READ_ONLY);
        query = "SELECT * FROM Daily_Events WHERE id = '"+id+"'";
        //System.out.println("\nExecuting query: " + query);
        rset = stmt.executeQuery(query); 
        return rset;
    }
    public ResultSet getEventFromCustomEvents(int id) throws SQLException{
        getDBConnection(); 
        stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
               ResultSet.CONCUR_READ_ONLY);
        query = "SELECT * FROM Custom_Events WHERE id = '"+id+"'";
        //System.out.println("\nExecuting query: " + query);
        rset = stmt.executeQuery(query); 
        return rset;
    }
    public ResultSet getEventFromCompletedEvents(int id) throws SQLException{
        getDBConnection(); 
        stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
               ResultSet.CONCUR_READ_ONLY);
        query = "SELECT * FROM Completed_Events WHERE id = '"+id+"'";
        //System.out.println("\nExecuting query: " + query);
        rset = stmt.executeQuery(query); 
        return rset;
    }
    
    public int DeleteEventFromDailyEvents(int id) throws SQLException{
        getDBConnection(); 
        query = "Delete from Daily_Events Where id = '"+id+"'";
        PreparedStatement preStatement =conn.prepareStatement(query);
        int result = preStatement.executeUpdate();
        return result;
    }
    public int DeleteEventFromCustomEvents(int id) throws SQLException{
        getDBConnection(); 
        query = "Delete from Custom_Events Where id = '"+id+"'";
        PreparedStatement preStatement =conn.prepareStatement(query);
        int result = preStatement.executeUpdate();
        return result;
    }
    public int DeleteEventFromCompletedEvents(int id) throws SQLException{
        getDBConnection(); 
        query = "Delete from Completed_Events Where id = '"+id+"'";
        PreparedStatement preStatement =conn.prepareStatement(query);
        int result = preStatement.executeUpdate();
        return result;
    }
    public void UpdateDailyEvents(Event event) throws SQLException{
        getDBConnection(); 
        query = "UPDATE Daily_Events SET name = ? ,hasReminder= ?,day = ? ,hour = ? ,minute = ?  WHERE ID = " + event.getId();
        PreparedStatement preStatement =conn.prepareStatement(query);
        preStatement.setString(1, event.getEventDisc());
        preStatement.setBoolean(2,event.getHasTime());
        if(event.getEventDate()==null)
        	 preStatement.setDate(3,null);
        else
        preStatement.setDate(3,new java.sql.Date( event.getEventDate().getTime()));
        preStatement.setInt(4, event.getHour());
        preStatement.setInt(5, event.getMinute());
        int result = preStatement.executeUpdate();
    }
    
    public void UpdateCustomEvents(Event event) throws SQLException{
        getDBConnection(); 
        query = "UPDATE Custom_Events SET name = ? ,hasReminder= ?,day = ? ,hour = ? ,minute = ? WHERE ID = "+event.getId();
        PreparedStatement preStatement =conn.prepareStatement(query);
        preStatement.setString(1, event.getEventDisc());
        preStatement.setBoolean(2,event.getHasTime());
        if(event.getEventDate()==null)
        	 preStatement.setDate(3,null);
        else
        preStatement.setDate(3,new java.sql.Date( event.getEventDate().getTime()));
        preStatement.setInt(4, event.getHour());
        preStatement.setInt(5, event.getMinute());
        int result = preStatement.executeUpdate();
    }
    public void UpdateCompletedEvents(Event event) throws SQLException{
        getDBConnection(); 
        query = "UPDATE Completed_Events SET name = ? ,hasReminder= ?,day = ? ,hour = ? ,minute = ? WHERE ID = "+event.getId();
        PreparedStatement preStatement =conn.prepareStatement(query);
        preStatement.setString(1, event.getEventDisc());
        preStatement.setBoolean(2,event.getHasTime());
        if(event.getEventDate()==null)
        	 preStatement.setDate(3,null);
        else
        preStatement.setDate(3,new java.sql.Date( event.getEventDate().getTime()));
        preStatement.setInt(4, event.getHour());
        preStatement.setInt(5, event.getMinute());
        int result = preStatement.executeUpdate();
    }
}
