package goba;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AddEventController {
	
	public MainController mainController;
	
	@FXML
	private Button BtnOk;
	@FXML
	private Button BtnCancel;
	@FXML
	private TextArea textArea;
	@FXML
	private CheckBox ReminderCheckBox;
	@FXML
	private DatePicker DatePicker;
	@FXML
	private TextField HourTextField;
	@FXML
	private TextField MinuteTextField;
	@FXML
	private RadioButton RadioBtnAm;
	@FXML
	private RadioButton RadioBtnPm;
	
	int SelectedIndex;
	int Action; // 1 for Create , 2 for Update
	int EventId;// in Editing Case this is the Event id being editing
	
	public AddEventController(MainController mainController,int action)
	{
		this.Action=action;
		this.mainController=mainController;
	}
	 public void initialize() throws Exception {
		 
		 BtnOk.setOnAction(new EventHandler<ActionEvent>() {
			 @Override
			 public void handle(ActionEvent arg0) {
				 try {
					BtnOkPressed();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 }	
		 });
		 BtnCancel.setOnAction(new EventHandler<ActionEvent>() {
			 @Override
			 public void handle(ActionEvent arg0) {
				 BtnCancelPressed();
			 }	
		 });	
	 }
	 public void BtnCancelPressed() {
		 Stage stg= (Stage)BtnCancel.getScene().getWindow();
		 stg.close();
		 }
	 public void BtnOkPressed() throws Exception {
		 validate();
		 Event event;
		 event= new Event();
		 event.setEventDisc(textArea.getText());
		 LocalDate localDate = DatePicker.getValue();
		 if(localDate!=null)
		 {
			 Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			 Date date = Date.from(instant);
			 event.setEventDate(date);
		 }
		 event.setHasTime(ReminderCheckBox.isSelected());
		 if(!HourTextField.getText().isBlank())
		 {
			 event.setHour(Integer.parseInt(HourTextField.getText()));
		 }
		 if(!MinuteTextField.getText().isBlank())
		 event.setMinute(Integer.parseInt(MinuteTextField.getText()));
		 
		 if(this.Action==1)
			 InsertEvent(event);
		 else
			 UpdateEvent(event);
		 
		 Stage stg= (Stage)BtnOk.getScene().getWindow();
		 stg.close();
		 this.mainController.RefreshSelcetedIndex(this.SelectedIndex);
	}
	public void UpdateEvent(Event event) {
		event.setId(this.EventId);
		ResultSet RS = null;
		GobaDatabase db=new GobaDatabase();
		 try {
			 if(this.SelectedIndex==0)
				db.UpdateDailyEvents(event);
			 else if(this.SelectedIndex==1)
				 db.UpdateCustomEvents(event);
			 else if(this.SelectedIndex==2)
				 db.UpdateCompletedEvents(event);
		 }
		 catch (SQLException e) {
			// TODO Auto-generated catch block
			 e.printStackTrace();
		 }
		
	}
	public void InsertEvent(Event event) {
		 
		GobaDatabase db=new GobaDatabase();
		 try {
			 if(this.SelectedIndex==0)
				 db.AddToDailyEvents(event);
			 else if(this.SelectedIndex==1)
				 db.AddToCustomEvents(event);
			 else if(this.SelectedIndex==2)
				 db.AddToCompletedEvents(event);
		 }
		 catch (SQLException e) {
			// TODO Auto-generated catch block
			 e.printStackTrace();
		 }
		
	}
	public void validate() {
		if(textArea.getText().isBlank())
			textArea.setText("");	
	}
	public void ShowWindow(int SelectedIndex)
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("add_event_layout.fxml"));
		loader.setController(this);
		Parent AddEventScene =null;
		try {
			 AddEventScene = (Parent)loader.load();
			 Stage primaryStage = new Stage();
			 primaryStage.setTitle("Add Event");
			 Scene scene = new Scene(AddEventScene);
			 primaryStage.initModality(Modality.APPLICATION_MODAL);
			 primaryStage.setScene(scene);
			 primaryStage.setResizable(false);
			 primaryStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.SelectedIndex=SelectedIndex;
	}
	
	public void DisplayEditingItemData(int id)
	{
		this.EventId=id;
		ResultSet RS = null;
		GobaDatabase db=new GobaDatabase();
		 try {
			 if(this.SelectedIndex==0)
				 RS=db.getEventFromDailyEvents(id);
			 else if(this.SelectedIndex==1)
				 RS=db.getEventFromCustomEvents(id);
			 else if(this.SelectedIndex==2)
				 RS=db.getEventFromCompletedEvents(id);
			 if(RS!=null)
				while (RS.next()) {
					textArea.setText(RS.getString(2));
				}
		 }
		 catch (SQLException e) {
			// TODO Auto-generated catch block
			 e.printStackTrace();
		 }
	}
}
