package goba;

import java.util.Date;

public class Event {
	
	private int Id;
	private Boolean isRunning;
	private Boolean isFinished;
	private Boolean hasReminder;
	private char  repeatType;
	private String EventDisc;
	private Date EventDate;
	private int Hour;
	private int Minute;
	
	public int getId() {
		return this.Id;
	}
	public void setId(int id) {
		this.Id = id;
	}
	
	public void setisRunning(Boolean v)
	{
		isRunning=v;
	}
	public void setisfinished(Boolean v)
	{
		isFinished=v;
	}
	
	public Boolean getisRunning()
	{
		return isRunning;
	}
	public Boolean getisfinished()
	{
		return isFinished;
	}
	public Boolean getHasTime() {
		return hasReminder;
	}
	public void setHasTime(Boolean hasReminder) {
		this.hasReminder = hasReminder;
	}
	public char getRepeatType() {
		return repeatType;
	}
	public void setRepeatType(char repeatType) {
		this.repeatType = repeatType;
	}
	public String getEventDisc() {
		return EventDisc;
	}
	public void setEventDisc(String eventDisc) {
		EventDisc = eventDisc;
	}
	public Date getEventDate() {
		return EventDate;
	}
	public void setEventDate(Date eventDate) {
		EventDate = eventDate;
	}
	public int getHour() {
		return Hour;
	}
	public void setHour(int hour) {
		Hour = hour;
	}
	public int getMinute() {
		return Minute;
	}
	public void setMinute(int minute) {
		Minute = minute;
	}
	public String toString() { 
	    return EventDisc;
	} 
	
}
