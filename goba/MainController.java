package goba;


import java.io.IOException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.sql.ResultSet;
import java.sql.SQLException;


public class MainController {

	@FXML
	private ListView<String> eventsNamesListId;
	@FXML
	private ListView<Event> eventsListId;
	@FXML
	private Button AddEvent;
    public void initialize() throws Exception {
    	
    	ObservableList<String>eventsNamesList=FXCollections.observableArrayList(
				"Daily Events","Custom Events","Completed Events");
		eventsNamesListId.setItems(eventsNamesList);
		ContextMenu contextMenu = new ContextMenu();
        MenuItem menuItem1 = new MenuItem("Edit");
        menuItem1.setOnAction((event) -> {
            Edit(eventsListId.getSelectionModel().getSelectedIndex());
        });
        MenuItem menuItem2 = new MenuItem("Delete");
        menuItem2.setOnAction((event) -> {
            Delete(eventsListId.getSelectionModel().getSelectedIndex(),eventsNamesListId.getSelectionModel().getSelectedIndex());
        });
        contextMenu.getItems().addAll(menuItem1,menuItem2);
        eventsListId.setContextMenu(contextMenu);
		eventsNamesListId.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		    	int index=eventsNamesListId.getSelectionModel().getSelectedIndex();
		        EventsListPressed(index);
		    }
		});
		eventsNamesListId.getFocusModel().focus(0);
		eventsNamesListId.getSelectionModel().select(0);
		AddEvent.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				AddButtonPressed();
				}	
		});
    }
    void AddButtonPressed() {
		AddEventController addEventController=new AddEventController(this,1);// 1 for Create
		addEventController.ShowWindow(eventsNamesListId.getSelectionModel().getSelectedIndex());
	}
    public void Edit(int index) {
    	AddEventController addEventController=new AddEventController(this,2);// 2 for Update
		addEventController.ShowWindow(eventsNamesListId.getSelectionModel().getSelectedIndex());
		int EventId=eventsListId.getItems().get(index).getId();
		addEventController.DisplayEditingItemData(EventId);
	}
    public void Delete(int index,int SelectedListIndex) {
    	int id=eventsListId.getItems().get(index).getId();
 
    	GobaDatabase db=new GobaDatabase();
		 try {
			 if(SelectedListIndex==0)
				 db.DeleteEventFromDailyEvents(id);
			 else if(SelectedListIndex==1)
				 db.DeleteEventFromCustomEvents(id);
			 else if(SelectedListIndex==2)
				 db.DeleteEventFromCompletedEvents(id);
		 } catch (Exception e) {
			// TODO: handle exception
		}
		 RefreshSelcetedIndex(SelectedListIndex);
	}
	public void ShowWindow()
    {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("main_layout.fxml"));
    	loader.setController(this);
		Parent MainScene =null;
		try {
			 MainScene = (Parent)loader.load();
			 Stage primaryStage = new Stage();
			 primaryStage.setTitle("Goba To Do List");
			 Scene scene = new Scene(MainScene);
			 primaryStage.setScene(scene);
			 primaryStage.setResizable(false);
			 primaryStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	void EventsListPressed(int index) {
		ObservableList<Event>eventsList=FXCollections.observableArrayList();
		GobaDatabase db=new GobaDatabase();
		try {
				ResultSet RS = null;
				if(index==0)
					RS=db.getDailyEvents();
				else if(index==1)
					RS=db.getCustomEvents();
				else if(index==2)
					RS=db.getCompletedEvents();
				if(RS!=null)
				while (RS.next()) {
					Event event = new Event();
					event.setId(Integer.parseInt(RS.getString(1)));
					event.setEventDisc(RS.getString(2));
					eventsList.add(event);
				}
				eventsListId.setItems(eventsList);	
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void RefreshSelcetedIndex(int index) {
		EventsListPressed(index);
	}
}